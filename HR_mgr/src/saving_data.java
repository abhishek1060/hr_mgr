import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JScrollBar;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class saving_data {

	private JFrame frame;
	private JTextField name;
	private JTextField contactno;
	private JTextField empid;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					saving_data window = new saving_data();
					window.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public saving_data() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 332);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAddANew = new JLabel("Add a new Employee");
		lblAddANew.setFont(new Font("Georgia", Font.BOLD, 14));
		lblAddANew.setBounds(160, 11, 182, 23);
		frame.getContentPane().add(lblAddANew);
		
		JLabel lblFullName = new JLabel("Full name:");
		lblFullName.setBounds(24, 63, 105, 14);
		frame.getContentPane().add(lblFullName);
		
		name = new JTextField();
		name.setToolTipText("enter the full name");
		name.setBounds(139, 60, 243, 20);
		frame.getContentPane().add(name);
		name.setColumns(10);
		
		JLabel lblGender = new JLabel("Gender:");
		lblGender.setBounds(22, 96, 46, 14);
		frame.getContentPane().add(lblGender);
		
		JRadioButton female = new JRadioButton("Female");
		female.setBounds(138, 92, 80, 23);
		frame.getContentPane().add(female);
		
		JRadioButton male = new JRadioButton("Male");
		male.setBounds(232, 92, 109, 23);
		frame.getContentPane().add(male);
		
		JLabel lblContactNo = new JLabel("Contact no:");
		lblContactNo.setBounds(24, 158, 105, 14);
		frame.getContentPane().add(lblContactNo);
		
		contactno = new JTextField();
		contactno.setToolTipText("enter the contact no");
		contactno.setBounds(139, 155, 166, 20);
		frame.getContentPane().add(contactno);
		contactno.setColumns(10);
		
		JLabel lblDateOfJoining = new JLabel("Date of joining:");
		lblDateOfJoining.setBounds(24, 192, 105, 14);
		frame.getContentPane().add(lblDateOfJoining);
		
		JComboBox select_date = new JComboBox();
		select_date.setBounds(149, 189, 52, 20);
		frame.getContentPane().add(select_date);
		
		JComboBox select_month = new JComboBox();
		select_month.setBounds(232, 189, 46, 20);
		frame.getContentPane().add(select_month);
		
		JComboBox select_year = new JComboBox();
		select_year.setBounds(307, 189, 75, 20);
		frame.getContentPane().add(select_year);
		
		JLabel lblD = new JLabel("D");
		lblD.setBounds(139, 192, 21, 14);
		frame.getContentPane().add(lblD);
		
		JLabel lblM = new JLabel("M");
		lblM.setBounds(219, 192, 22, 14);
		frame.getContentPane().add(lblM);
		
		JLabel lblY = new JLabel("Y");
		lblY.setBounds(297, 192, 21, 14);
		frame.getContentPane().add(lblY);
		
		JLabel lblEmpId = new JLabel("Emp Id:");
		lblEmpId.setBounds(24, 124, 46, 23);
		frame.getContentPane().add(lblEmpId);
		
		empid = new JTextField();
		empid.setToolTipText("enter the employee id");
		empid.setBounds(139, 122, 108, 20);
		frame.getContentPane().add(empid);
		empid.setColumns(10);
		
		JButton btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String fname=name.getText();
				System.out.println("Full name is:"+fname);
				
				if(male.isSelected()){
					System.out.println("male selected");
				}else if(female.isSelected()){
				    System.out.println("female slected");
				    }
				String emplid=empid.getText();
				String cntno=contactno.getText();
				int getdate=(int) select_date.getSelectedItem();
				
				int getmonth=(int) select_month.getSelectedItem();
				int getyear=(int) select_year.getSelectedItem();
				
			}
		});
		
		//Creating array list for date month and year
		List<Integer> listDate;
		try {
			listDate = new ArrayList<>();
			for(int i=1;i<=31;i++){
				listDate.add(i);
			}
			for(int j=0;j<listDate.size();j++){
				select_date.addItem(listDate.get(j));
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		List<Integer> listMonth;
		try {
			listMonth = new ArrayList<>();
			for(int k=1;k<13;k++){
				listMonth.add(k);
			}
			for(int a=0;a<listMonth.size();a++){
				select_month.addItem(listMonth.get(a));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<Integer> listYear;
		try {
			listYear = new ArrayList<>();
			for(Integer m=2016;m<2050;m++){
				listYear.add(m);
			}
			for(int b=0;b<listYear.size();b++){
				select_year.addItem(listYear.get(b));
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
		btnInsert.setBounds(192, 241, 89, 23);
		frame.getContentPane().add(btnInsert);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				name.setText(null);
				empid.setText(null);
				male.setSelected(false);
				female.setSelected(false);
				contactno.setText(null);
				select_date.setSelectedItem(1);
				select_month.setSelectedItem(1);
				select_year.setSelectedItem(2016);
			}
		});
		btnReset.setBounds(305, 241, 89, 23);
		frame.getContentPane().add(btnReset);
	}
}
